package com.snegrete.wallet.account.service;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.catchThrowable;

import java.math.BigDecimal;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import com.snegrete.wallet.account.Account;
import com.snegrete.wallet.account.Transaction;

@SpringBootTest
@RunWith(SpringRunner.class)
public class MonetaryTransacctionService_Transactions_Test {
	
	@Autowired
	private MonetaryTransactionService monetaryTransactionService;
	
	@Test
	public void aHundredTransactionTest() {
		Account originAccount = new Account();
		originAccount.setBalance(BigDecimal.valueOf(500));
		Account destinyAccount = new Account();
		destinyAccount.setBalance(BigDecimal.valueOf(50));
		
		Transaction transaction = new Transaction(originAccount, destinyAccount);
		
		transaction = monetaryTransactionService.doTransaction(transaction, BigDecimal.valueOf(100));

		assertThat(originAccount.getBalance()).isEqualByComparingTo(BigDecimal.valueOf(400));
		assertThat(destinyAccount.getBalance()).isEqualByComparingTo(BigDecimal.valueOf(150));
	}

	@Test
	public void negativeTransactionTest() {
		Account originAccount = new Account();
		originAccount.setBalance(BigDecimal.valueOf(500));
		Account destinyAccount = new Account();
		destinyAccount.setBalance(BigDecimal.valueOf(50));
		
		Transaction transaction = new Transaction(originAccount, destinyAccount);
		

		Throwable e = catchThrowable(() -> monetaryTransactionService.doTransaction(transaction, BigDecimal.valueOf(-100)));
			
		assertThat(e)
			.isInstanceOf(IllegalArgumentException.class)
			.withFailMessage(MonetaryTransactionService.ZERO_ERROR_MSG);
		assertThat(originAccount.getBalance()).isEqualByComparingTo(BigDecimal.valueOf(500));
		assertThat(destinyAccount.getBalance()).isEqualByComparingTo(BigDecimal.valueOf(50));
	}

	@Test
	public void limitTransactionTest() {
		Account originAccount = new Account();
		originAccount.setBalance(BigDecimal.valueOf(3500));
		Account destinyAccount = new Account();
		destinyAccount.setBalance(BigDecimal.valueOf(50));
		
		Transaction transaction = new Transaction(originAccount, destinyAccount);
		transaction = monetaryTransactionService.doTransaction(transaction, BigDecimal.valueOf(3000));

		assertThat(originAccount.getBalance()).isEqualByComparingTo(BigDecimal.valueOf(500));
		assertThat(destinyAccount.getBalance()).isEqualByComparingTo(BigDecimal.valueOf(3050));
	}

	@Test
	public void limitOverflowTransactionTest() {
		Account originAccount = new Account();
		originAccount.setBalance(BigDecimal.valueOf(3500));
		Account destinyAccount = new Account();
		destinyAccount.setBalance(BigDecimal.valueOf(50));
		
		Transaction transaction = new Transaction(originAccount, destinyAccount);

		Throwable e = catchThrowable(() -> monetaryTransactionService.doTransaction(transaction, BigDecimal.valueOf(-100)));
			
		assertThat(e)
			.isInstanceOf(IllegalArgumentException.class)
			.withFailMessage(MonetaryTransactionService.TRANSFER_LIMIT_ERROR_MESSAGE);
		assertThat(originAccount.getBalance()).isEqualByComparingTo(BigDecimal.valueOf(3500));
		assertThat(destinyAccount.getBalance()).isEqualByComparingTo(BigDecimal.valueOf(50));
	}
}
