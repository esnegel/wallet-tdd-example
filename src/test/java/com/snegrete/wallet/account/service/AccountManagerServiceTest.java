package com.snegrete.wallet.account.service;

import static org.assertj.core.api.Assertions.assertThat;

import java.math.BigDecimal;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import com.snegrete.wallet.account.Account;

@SpringBootTest
@RunWith(SpringRunner.class)
public class AccountManagerServiceTest {
	
	@Autowired
	private AccountManagerService accountCreationService;
	
	@Test
	public void createAccountTest() {
		Account account = accountCreationService.createAccount();
	
		assertThat(account.getBalance()).isEqualByComparingTo(BigDecimal.ZERO);
	}
}
