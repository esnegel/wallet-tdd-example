package com.snegrete.wallet.account.service;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.catchThrowable;

import java.math.BigDecimal;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import com.snegrete.wallet.account.Account;

@SpringBootTest
@RunWith(SpringRunner.class)
public class MonetaryTransacctionService_Withdraw_Test {
	
	@Autowired
	private MonetaryTransactionService monetaryTransactionService;
	
	@Test
	public void aHundredWithdraw_FiveHundredAccountTest() {
		Account account = new Account();
		account.setBalance(BigDecimal.valueOf(500));
		
		account = monetaryTransactionService.doWithdraw(account, BigDecimal.valueOf(100));

		assertThat(account.getBalance()).isEqualByComparingTo(BigDecimal.valueOf(400));
	}
	
	@Test
	public void fiveHundredWithdraw_TwoHundredAccountTest() {
		Account account = new Account();
		account.setBalance(BigDecimal.valueOf(200));
		
		Throwable e = catchThrowable(() -> monetaryTransactionService.doWithdraw(account, BigDecimal.valueOf(500)));
			
		assertThat(e)
			.isInstanceOf(IllegalArgumentException.class)
			.withFailMessage(MonetaryTransactionService.NOT_ENOUGH_ERROR);
		assertThat(account.getBalance()).isEqualByComparingTo(BigDecimal.valueOf(200));
	}

	@Test
	public void negativeWithdrawTest() {
		Account account = new Account();
		account.setBalance(BigDecimal.valueOf(500));

		Throwable e = catchThrowable(() -> monetaryTransactionService.doWithdraw(account, BigDecimal.valueOf(-100)));
			
		assertThat(e)
			.isInstanceOf(IllegalArgumentException.class)
			.withFailMessage(MonetaryTransactionService.ZERO_ERROR_MSG);
		assertThat(account.getBalance()).isEqualByComparingTo(BigDecimal.valueOf(500));
	}

	@Test
	public void twoDecimalWithdrawTest() {
		Account account = new Account();
		account.setBalance(BigDecimal.valueOf(500));
		account = monetaryTransactionService.doWithdraw(account, BigDecimal.valueOf(100.45));

		assertThat(account.getBalance()).isEqualByComparingTo(BigDecimal.valueOf(399.55));
	}

	@Test
	public void threeDecimalWithdrawTest() {
		Account account = new Account();
		account.setBalance(BigDecimal.valueOf(500));

		Throwable e = catchThrowable(() -> monetaryTransactionService.doWithdraw(account, BigDecimal.valueOf(100.457)));
			
		assertThat(e)
			.isInstanceOf(IllegalArgumentException.class)
			.withFailMessage(MonetaryTransactionService.SCALE_ERROR_MSG);
		assertThat(account.getBalance()).isEqualByComparingTo(BigDecimal.valueOf(500));
	}

	@Test
	public void maxWithdrawTest() {
		Account account = new Account();
		account.setBalance(BigDecimal.valueOf(7000));
		account = monetaryTransactionService.doWithdraw(account, MonetaryTransactionService.MAX_DEPOSIT);

		assertThat(account.getBalance()).isEqualByComparingTo(BigDecimal.valueOf(1000));
	}

	@Test
	public void maxOverflowWithdrawTest() {
		Account account = new Account();
		account.setBalance(BigDecimal.valueOf(7000));

		Throwable e = catchThrowable(() -> monetaryTransactionService.doWithdraw(account, MonetaryTransactionService.MAX_DEPOSIT.add(BigDecimal.valueOf(0.01))));
			
		assertThat(e)
			.isInstanceOf(IllegalArgumentException.class)
			.withFailMessage(MonetaryTransactionService.SCALE_ERROR_MSG);
		assertThat(account.getBalance()).isEqualByComparingTo(BigDecimal.valueOf(7000));
	}
}
