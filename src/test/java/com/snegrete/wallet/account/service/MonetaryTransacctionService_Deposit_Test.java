package com.snegrete.wallet.account.service;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.catchThrowable;

import java.math.BigDecimal;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import com.snegrete.wallet.account.Account;

@SpringBootTest
@RunWith(SpringRunner.class)
public class MonetaryTransacctionService_Deposit_Test {
	
	@Autowired
	private MonetaryTransactionService monetaryTransactionService;
	
	@Test
	public void aHundredInEmptyAccountTest() {
		Account account = new Account();
		account = monetaryTransactionService.doDeposit(account, BigDecimal.valueOf(100));

		assertThat(account.getBalance()).isEqualByComparingTo(BigDecimal.valueOf(100));
	}

	@Test
	public void threeThousandInEmptyAccountTest() {
		Account account = new Account();
		account = monetaryTransactionService.doDeposit(account, BigDecimal.valueOf(3000));

		assertThat(account.getBalance()).isEqualByComparingTo(BigDecimal.valueOf(3000));
	}

	@Test
	public void threeThousandWithAHundredAccountTest() {
		Account account = new Account();
		account.setBalance(BigDecimal.valueOf(100));
		account = monetaryTransactionService.doDeposit(account, BigDecimal.valueOf(3000));

		assertThat(account.getBalance()).isEqualByComparingTo(BigDecimal.valueOf(3100));
	}

	@Test
	public void negativeDepositTest() {
		Account account = new Account();
		
		Throwable e = catchThrowable(() -> monetaryTransactionService.doDeposit(account, BigDecimal.valueOf(-100)));
			
		assertThat(e)
			.isInstanceOf(IllegalArgumentException.class)
			.withFailMessage(MonetaryTransactionService.ZERO_ERROR_MSG);
		assertThat(account.getBalance()).isEqualByComparingTo(BigDecimal.ZERO);
	}

	@Test
	public void twoDecimalDepositTest() {
		Account account = new Account();
		account = monetaryTransactionService.doDeposit(account, BigDecimal.valueOf(100.45));

		assertThat(account.getBalance()).isEqualByComparingTo(BigDecimal.valueOf(100.45));
	}

	@Test
	public void threeDecimalDepositTest() {
		Account account = new Account();

		Throwable e = catchThrowable(() -> monetaryTransactionService.doDeposit(account, BigDecimal.valueOf(100.457)));
		
		assertThat(e)
			.isInstanceOf(IllegalArgumentException.class)
			.withFailMessage(MonetaryTransactionService.ZERO_ERROR_MSG);
		assertThat(account.getBalance()).isEqualByComparingTo(BigDecimal.ZERO);
	}

	@Test
	public void maxDepositTest() {
		Account account = new Account();
		account = monetaryTransactionService.doDeposit(account, MonetaryTransactionService.MAX_DEPOSIT);

		assertThat(account.getBalance()).isEqualByComparingTo(MonetaryTransactionService.MAX_DEPOSIT);
	}

	@Test
	public void maxOverflowDepositTest() {
		Account account = new Account();

		Throwable e = catchThrowable(() -> monetaryTransactionService.doDeposit(account, MonetaryTransactionService.MAX_DEPOSIT.add(BigDecimal.valueOf(0.01))));
		
		assertThat(e)
			.isInstanceOf(IllegalArgumentException.class)
			.withFailMessage(MonetaryTransactionService.ZERO_ERROR_MSG);
		assertThat(account.getBalance()).isEqualByComparingTo(BigDecimal.ZERO);
	}
}
