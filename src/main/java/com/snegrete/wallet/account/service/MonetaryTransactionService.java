package com.snegrete.wallet.account.service;

import java.math.BigDecimal;

import org.springframework.stereotype.Service;
import org.springframework.util.Assert;

import com.snegrete.wallet.account.Account;
import com.snegrete.wallet.account.Transaction;

@Service
public class MonetaryTransactionService {
	
	public static final BigDecimal MAX_DEPOSIT = BigDecimal.valueOf(6000.00);
	public static final BigDecimal TRANSFER_LIMIT = BigDecimal.valueOf(3000.00);

	public static final String ZERO_ERROR_MSG = "The Money to deposit must be greater than zero.";
	public static final String SCALE_ERROR_MSG = "The Money scale must not be greater than 2 digits.";
	public static final String MAX_DEPOSIT_ERROR_MESSAGE = String.format("The maximun money is %f", MAX_DEPOSIT);
	public static final String TRANSFER_LIMIT_ERROR_MESSAGE = String.format("The transfer limit is %f", TRANSFER_LIMIT);
	public static final String NOT_ENOUGH_ERROR = "There is not enough money in the account.";
	
	public Account doDeposit(Account account, BigDecimal money) {
		moneyAssertions(money);
		account.setBalance(account.getBalance().add(money));
		return account;
	}

	public Account doWithdraw(Account account, BigDecimal money) {
		moneyAssertions(money);
		Assert.isTrue(money.compareTo(account.getBalance()) < 1, NOT_ENOUGH_ERROR);
		account.setBalance(account.getBalance().subtract(money));
		return account;
	}

	public Transaction doTransaction(Transaction transaction, BigDecimal money) {
		Assert.isTrue(TRANSFER_LIMIT.compareTo(money) > -1, TRANSFER_LIMIT_ERROR_MESSAGE);
		transaction.setOriginAccount(this.doWithdraw(transaction.getOriginAccount(), money));
		transaction.setDestinyAccount(this.doDeposit(transaction.getDestinyAccount(), money));
		return transaction;
	}
	
	private void moneyAssertions(BigDecimal money) {
		Assert.isTrue(BigDecimal.ZERO.compareTo(money) < 1, ZERO_ERROR_MSG);
		Assert.isTrue(money.scale() < 3, SCALE_ERROR_MSG);
		Assert.isTrue(MAX_DEPOSIT.compareTo(money) > -1, MAX_DEPOSIT_ERROR_MESSAGE);
	}
}
