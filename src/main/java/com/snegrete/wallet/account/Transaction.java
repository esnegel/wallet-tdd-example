package com.snegrete.wallet.account;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

@AllArgsConstructor
public class Transaction {

	@Getter @Setter
	private Account originAccount;
	
	@Getter @Setter
	private Account destinyAccount;
}
