package com.snegrete.wallet.account;

import java.math.BigDecimal;

import org.springframework.format.annotation.NumberFormat;
import org.springframework.format.annotation.NumberFormat.Style;

import lombok.Getter;
import lombok.Setter;

public class Account {

	@Getter @Setter @NumberFormat(style = Style.CURRENCY)
	private BigDecimal balance = new BigDecimal(0);
}
